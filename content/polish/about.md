---
title : "Centrum Obliczeń <br> i Modelowania Komputerowego"
image : "images/logo.png"


buttonRegulamin:
  enable : true
  label : "Regulamin COiMK"
  link : "https://bip.ujk.edu.pl/zarzadzenie_nr_99_2021.html"


########################### Jak założyć konto ##############################
experience:
  enable : true
  title : "Jak uzyskać dostęp"
  experience_list:
    # experience item loop
    - name : "Zapoznaj się z regulaminem użytkownika"
      company : ""
      content : "[Regulamin Użytkownika](/docs/RegulaminUzytkownika.pdf)"
      
    # experience item loop
    - name : "Wypełnij wniosek"
      company : ""
      content : "[Wniosek](/docs/wniosek_v1.pdf)"
      
    # experience item loop
    - name : "Wyślij wypełniony wniosek na adres coimk@ujk.edu.pl"
      
    # experience item loop
    - name : "W przypadku akceptacji wniosku, skontaktuj się z administratorem (admin.coimk@ujk.edu.pl) w celu utworzenia konta użytkownika."

############################### Skill #################################
skill:
  enable : true
  title : "Statystyki"
  skill_list:
    # skill item loop
    - name : "Dostępność usług"
      percentage : "98%"
      
    # skill item loop
    - name : "Średnie obciążenie CPU"
      percentage : "85%"
      
    # skill item loop
    - name : "Średnie obciążenie RAM"
      percentage : "90%"
      


# custom style
custom_class: "" 
custom_attributes: "" 
custom_css: ""
---

Centrum Obliczeń i Modelowania Komputerowego (COiMK) jest samodzielną jednostką funkcjonującą w strukturze Wydziału Nauk Ścisłych i Przyrodniczych 
Uniwersytetu Jana Kochanowskiego w Kielcach (UJK). Centrum zostało utworzone na mocy decyzji Rektora UJK i pozwala na zintegrowanie zasobów 
komputerowych Wydziału, tak aby stanowiły urządzenie o dużej mocy obliczeniowej. 
Aktualnie do budowy infrastruktury Centrum wykorzystano zasoby
przynależne do Instytutu Fizyki oraz Katedry Matematyki.

Centrum jest jednostką naukowo-badawczą, której celem jest prowadzenie badań naukowych. Misją Centrum jest budowanie wartości dla nauki 
i społeczeństwa poprzez tworzenie i dostarczanie nowatorskich rozwiązań informatycznych i obliczeniowych w oparciu o interdyscyplinarne 
podejście, współpracę, kompetencje i infrastrukturę.

