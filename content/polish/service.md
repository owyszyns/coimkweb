---
title : "Dostępne Oprogramowanie"
service_list:
# service item loop
- name : "Python"
  image : "images/icons/python.jpeg"
  
# service item loop
- name : "C, C++"
  image : "images/icons/c-logo.png"
  
# service item loop
- name : "GFortran"
  image : "images/icons/fortran.png"
  
# service item loop
- name : "Root"
  image : "images/icons/root-logo.png"
  
# service item loop
- name : "Geant4"
  image : "images/icons/g4logo.png"
  
# service item loop
- name : "FreeFEM"
  image : "images/icons/FreeFEM.svg"



# custom style
custom_class: "" 
custom_attributes: "" 
custom_css: ""
---
