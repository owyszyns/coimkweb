---
title : "Centrum Obliczeń i Modelowania Komputerowego"
# full screen navigation
first_name : ""
last_name : ""
bg_image : "images/slider/slider-2.jpg"
# animated text loop
occupations:
- "Ponad 1000 rdzeni obliczeniowych"
- "100 GB dla każdego użytkownika"
- "Duży wybór preinstalowanego oprogramowania"
- "Dostęp poprzez SSH"

# slider background image loop
slider_images:
- "images/slider/slider-1.jpg"
- "images/slider/slider-2.jpg"
- "images/slider/slider-3.jpg"
- "images/slider/slider-4.jpg"

# button
button:
  enable : true
  label : "Więcej"
  link : "#about"


# custom style
custom_class: "" 
custom_attributes: "" 
custom_css: ""

---
