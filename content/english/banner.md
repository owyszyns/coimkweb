---
title : "Computing and Modeling Center"
# full screen navigation
first_name : ""
last_name : ""
#bg_image : "images/backgrounds/full-nav-bg.jpg"
# animated text loop
occupations:
- "Over 100 computing cores"
- "100 GB for each user"
- "Broad selection of scientific software"
- "Access via SSH"

# slider background image loop
slider_images:
- "images/slider/slider-1.jpg"
- "images/slider/slider-2.jpg"
- "images/slider/slider-3.jpg"

# button
button:
  enable : true
  label : "More"
  link : "#contact"


# custom style
custom_class: "" 
custom_attributes: "" 
custom_css: ""

---
